name := "ItemBasedCollaborativeFiltering"

version := "1.0"

organization := "com.hsahu"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
"org.apache.spark" %% "spark-core" % "2.0.0" % "provided",
"org.scala-lang" % "scala-library" % "2.10.1"
)
