package com.hsahu.scala.worksheet

import scala.util.matching.Regex

/**
* Scala workspace for regular expressions
**/
object scalaWorksheetRegex {
  
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  val pattern = new Regex("(S|s)cala")            //> pattern  : scala.util.matching.Regex = (S|s)cala

  val str = "Scala is scalable and cool"          //> str  : String = Scala is scalable and cool
  
  println((pattern findAllIn str).mkString(", ")) //> Scala, scala

  println(pattern findFirstIn str)                //> Some(Scala)

  println(pattern replaceFirstIn (str, "Java"))   //> Java is scalable and cool
  
  println(pattern replaceAllIn (str, "Java"))     //> Java is Javable and cool
  
  // We create a String and call the r( ) method on it.
  // Scala implicitly converts the String to a RichString and invokes that method to get an instance of Regex.
  // You can make use of the mkString( ) method to concatenate the resulting list
  val anotherPattern = "(S|s)cala".r              //> anotherPattern  : scala.util.matching.Regex = (S|s)cala
  
}