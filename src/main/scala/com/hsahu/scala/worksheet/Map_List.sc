package com.hsahu.scala.worksheet

object scalaWorksheet_DS_2 {
	// more list oeprations
  val list = List(6, 7, 8, 4, 3)                  //> list  : List[Int] = List(6, 7, 8, 4, 3)
  val extendedList = List(1, 4, 3)                //> extendedList  : List[Int] = List(1, 4, 3)

  // concat list
  val newList = extendedList ++ list              //> newList  : List[Int] = List(1, 4, 3, 6, 7, 8, 4, 3)

  println(newList.reverse)                        //> List(3, 4, 8, 7, 6, 3, 4, 1)
  println(newList.sorted)                         //> List(1, 3, 3, 4, 4, 6, 7, 8)
  println(newList.distinct)                       //> List(1, 4, 3, 6, 7, 8)
  println(newList.max)                            //> 8
  println(newList.min)                            //> 1
  println(newList.sum)                            //> 36
  println(newList.contains(4))                    //> true
  
  
  // Maps - similar to Java
  // useful for key/value lookups on distinct keys
  val someMap = Map("Himanshu" -> "hsahu", "GTS1321" -> "138", "Bengaluru" -> "560103", "Walter White" -> "Jessy Pinkman")
                                                  //> someMap  : scala.collection.immutable.Map[String,String] = Map(Himanshu -> h
                                                  //| sahu, GTS1321 -> 138, Bengaluru -> 560103, Walter White -> Jessy Pinkman)
	println(someMap("Himanshu"))              //> hsahu
	println(someMap.contains("Walter White")) //> true
	println(someMap.contains("Jon Snow"))     //> false
	
	val drugDealer = util.Try(someMap("Jessy Pinkman")) getOrElse("Jessy is not a drug dealer")
                                                  //> drugDealer  : String = Jessy is not a drug dealer
	val drugPartner = util.Try(someMap("Walter White")) getOrElse("null")
                                                  //> drugPartner  : String = Jessy Pinkman
}