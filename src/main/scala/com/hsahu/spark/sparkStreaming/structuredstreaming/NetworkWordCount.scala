package com.hsahu.spark.sparkStreaming.structuredstreaming

import org.apache.log4j._
import org.apache.spark.sql.SparkSession

object NetworkWordCount {

  def setupLogging() {
    Logger.getLogger("org").setLevel(Level.ERROR)
  }

  def main(args: Array[String]) {

    val sparkSession = SparkSession.builder
      .appName("Structured Streaming Network Word Count")
      .master("local[3]")
      .getOrCreate()

    setupLogging()

    import sparkSession.implicits._

    // Create DataFrame representing the stream of input lines from connection to localhost:9999
    val lines = sparkSession.readStream
      .format("socket")
      .option("host", "localhost")
      .option("port", 9999)
      .load()

    // Split the lines into words
    val words = lines.as[String].flatMap(x => x.split(" "))

    /**
     * This lines DataFrame represents an unbounded table containing the streaming text data.
     * This table contains one column of strings named “value”, and each line in the streaming text data becomes a row in the table.
     * Note, that this is not currently receiving any data as we are just setting up the transformation, and have not yet started it.
     * Next, we have converted the DataFrame to a Dataset of String using .as[String], so that we can apply the flatMap operation to split each line into multiple words.
     * The resultant words Dataset contains all the words.
     * Finally, we have defined the wordCounts DataFrame by grouping by the unique values in the Dataset and counting them.
     * Note that this is a streaming DataFrame which represents the running word counts of the stream.
     */

    // Generate running word count
    val wordCounts = words.groupBy("value").count()

    /**
     * We have now set up the query on the streaming data.
     * All that is left is to actually start receiving data and computing the counts.
     * To do this, we set it up to print the complete set of counts (specified by outputMode("complete")) to the console every time they are updated.
     * And then start the streaming computation using start().
     */

    // Start running the query that prints the running counts to the console
    val query = wordCounts.writeStream
      .outputMode("complete")
      .format("console")
      .start()

    /**
     * After this code is executed, the streaming computation will have started in the background.
     * The query object is a handle to that active streaming query, and we have decided to wait for
     * the termination of the query using awaitTermination() to prevent the process from exiting while the query is active.
     */

    query.awaitTermination()

    sparkSession.stop()
  }
}