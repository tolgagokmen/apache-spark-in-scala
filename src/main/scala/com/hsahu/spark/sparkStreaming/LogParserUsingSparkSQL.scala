package com.hsahu.spark.sparkStreaming

import org.apache.log4j._
import org.apache.spark.streaming.StreamingContext
import org.apache.spark.streaming.Seconds
import org.apache.spark.SparkConf
import java.util.regex.Pattern
import org.apache.spark.storage.StorageLevel
import org.apache.spark.sql.SQLContext
import org.apache.spark.SparkContext

object LogParserUsingSparkSQL {

  /**
   * get realtime logs from localhost socket
   */
  val hostName: String = "localhost"

  /**
   * get realtime logs from localhost socket at port
   */
  val port: Int = 9999

  /**
   * storage level of incoming data. Here we will store logs in both memory and disk in serializable format
   */
  val storageLevel: StorageLevel = StorageLevel.MEMORY_AND_DISK_SER

  /**
   *  Retrieves a regex Pattern for parsing Apache access logs.
   */
  def apacheLogPattern(): Pattern = {
    val ddd = "\\d{1,3}"
    val ip = s"($ddd\\.$ddd\\.$ddd\\.$ddd)?"
    val client = "(\\S+)"
    val user = "(\\S+)"
    val dateTime = "(\\[.+?\\])"
    val request = "\"(.*?)\""
    val status = "(\\d{3})"
    val bytes = "(\\S+)"
    val referer = "\"(.*?)\""
    val agent = "\"(.*?)\""
    val regex = s"$ip $client $user $dateTime $request $status $bytes $referer $agent"
    Pattern.compile(regex)
  }

  def setupLogging() {
    Logger.getRootLogger.setLevel(Level.ERROR)
  }

  def main(args: Array[String]) {

    val sparkConf = new SparkConf().setAppName("LogParserUsingSparkSQL").setMaster("local[*]")

    val streamingContext = new StreamingContext(sparkConf, Seconds(1))

    setupLogging()

    val pattern = apacheLogPattern()

    val lines = streamingContext.socketTextStream(hostName, port, storageLevel)

    /**
     * Extract (URL, httpStatus, agent) from each log line
     */
    val requests = lines.map(log => {
      val matcher = pattern.matcher(log)
      if (matcher.matches()) {
        val request = matcher.group(5)
        val requestFields = request.split(" ")
        val url = util.Try(requestFields(1)) getOrElse ("[ERROR]")
        (url, matcher.group(6).toInt, matcher.group(9))
      } else {
        ("[ERROR]", 0, "[ERROR]")
      }
    })

    /**
     * process batch of data as it comes in
     */
    requests.foreachRDD((rdd, time) => {

      /**
       * Get the singleton instance of SQLContext
       */
      val sqlContext = SQLContextSingleton.getInstance(rdd.sparkContext)

      import sqlContext.implicits._

      /**
       * SparkSQL can automatically create DataFrames from Scala "case classes".
       * We created the Record case class for this purpose.
       * So we'll convert each RDD of tuple data into an RDD of "Record"
       * objects, which in turn we can convert to a DataFrame using toDS()
       */
      val requestDataframes = rdd.map(x => new Record(x._1, x._2, x._3)).toDF()

      /**
       * Create a SQL table from this DataFrame
       */
      requestDataframes.createOrReplaceTempView("requests")

      /**
       * Count up occurrences of each user agent in this RDD and print the results.
       *  The powerful thing is that you can do any SQL you want here!
       *  But remember it's only querying the data in this RDD, from this batch.
       */
      val wordCountsDataFrame = sqlContext.sql("select agent, count(*) as total from requests group by agent")

      if (wordCountsDataFrame.count() > 0) {
        wordCountsDataFrame.write.json("output/log_" + time.milliseconds)
      }

      println(s"========= $time =========")

      wordCountsDataFrame.show()

      /**
       * If you want to dump data into an external database instead, check out the org.apache.spark.sql.DataFrameWriter class!
       * It can write data frames via JDBC and many other formats!
       * You can use the "append" save mode to keep adding data from each batch.
       */
    })

    streamingContext.start()

    streamingContext.awaitTermination()
  }
}

/**
 * Case class for converting RDD to DataFrame
 */
case class Record(url: String, status: Int, agent: String)

/**
 * Lazily instantiated singleton instance of SQLContext
 */
object SQLContextSingleton {

  @transient private var instance: SQLContext = _

  def getInstance(sparkContext: SparkContext): SQLContext = {
    if (instance == null) {
      instance = new SQLContext(sparkContext)
    }
    instance
  }
}






