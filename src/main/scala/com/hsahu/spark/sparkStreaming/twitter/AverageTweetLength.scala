package com.hsahu.spark.sparkStreaming.twitter

import org.apache.spark._
import org.apache.spark.streaming._
import org.apache.spark.streaming.twitter._
import Utils._
import org.apache.spark.streaming.dstream.ReceiverInputDStream
import org.apache.spark.streaming.dstream.DStream
import java.util.concurrent.atomic.AtomicLong

object AverageTweetLength {

  def main(args: Array[String]) {

    setupTwitter()

    val sparkConf = new SparkConf().setAppName("AverageTweetLength").setMaster("local[*]")

    val streamingContext = new StreamingContext(sparkConf, Seconds(1))

    setupLogging()

    val tweets = TwitterUtils.createStream(streamingContext, None)

    val statuses = tweets.map(status => status.getText())

    val lengths = statuses.map(status => status.length())

    var totalTweets = new AtomicLong(0)

    var totalChars = new AtomicLong(0)

    lengths.foreachRDD((rdd, time) => {

      var count = rdd.count()

      if (count > 0) {

        totalTweets.getAndAdd(count)

        totalChars.getAndAdd(rdd.reduce((x, y) => x + y))

        println("Total tweets: " + totalTweets.get() + " Total characters: " + totalChars.get() + " Average: " + totalChars.get / totalTweets.get)
      }
    })
    
    streamingContext.checkpoint("data/twitter/checkpoints")

    streamingContext.start()

    streamingContext.awaitTermination()
  }
}