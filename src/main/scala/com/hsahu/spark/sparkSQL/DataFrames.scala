package com.hsahu.spark.sparkSQL

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.rdd._
import org.apache.spark.sql._
import org.apache.log4j._

object DataFrames {
  /*these will be named column on sparkSQL*/
  case class Person(ID: Int, name: String, age: Int, numFriends: Int)

  def mapper(line: String): Person = {
    val fields = line.split(",")
    val person: Person = Person(fields(0).toInt, fields(1), fields(2).toInt, fields(3).toInt)
    return person
  }

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sparkSession: SparkSession = SparkSession.builder().appName("SparkSQL").master("local[*]").getOrCreate();

    val peopleRDD = sparkSession.sparkContext.textFile("data/fakefriends.csv").map(mapper)

    /*Infer the schema and register the DataSet as a table.*/
    import sparkSession.implicits._

    val people = peopleRDD.toDS().cache()

    /* Creates a new temporary table/view using a SparkDataFrame in the Spark Session.
     * If a temporary view with the same name already exists, replaces it.*/
    people.createOrReplaceTempView("People")

    println("People Schema: ")
    people.printSchema()

    println("People specific column: ")
    people.select("name").show()

    println("Teanagers: ")
    // SELECT * FROM people WHERE age >= 13 and age <= 19  
    people.filter(people("age") >= 13 && people("age") <= 19).show()

    println("Group by age: ")
    // SELECT count(*) FROM people group by age
    people.groupBy("age").count().show()

    println("Make Everyone 10 year older: ")
    people.select(people("name"), people("age") + 10).show()

    sparkSession.stop()
  }
}