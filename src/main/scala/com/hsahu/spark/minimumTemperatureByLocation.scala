package com.hsahu.spark

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._

object minimumTemperatureByLocation {

  def parseLine(line: String) = {
    val fields = line.split(",")
    val stationId = fields(0)
    val entryType = fields(2)
    val temperature = fields(3).toFloat * 0.1f * (9.0f / 5.0f) + 32.0f
    (stationId, entryType, temperature)
  }

  def main(args: Array[String]) {
    /**
     * Set the log level to only print errors
     */
    Logger.getLogger("org").setLevel(Level.ERROR)

    /**
     * Initialize Spark Context
     * Create a SparkContext using every core of the local machine, named minimumTemperatureByLocation
     * cluster name & job name
     */
    val sparkContext = new SparkContext("local[*]", "minimumTemperatureByLocation")

    /**
     * Load up each line of the ratings data into an RDD
     */
    val lines = sparkContext.textFile("data/weatherdata.csv")

    /**
     * parse raw line to create tuple3 with useful data (stationId, entryType, temperature)
     */
    val parsedLines = lines.map(parseLine)

    /**
     * filter all the line which don't have entryType as TMIN
     */
    val minTempRDD = parsedLines.filter(x => x._2 == "TMIN")

    /**
     * 	 Create a key/value RDD (stationId, temperature)
     */
    val minTempTupleRDD = minTempRDD.map(x => (x._1, x._3.toFloat))

    /**
     * get minimum of all the temperature of same station id
     */
    val minTempByStationRDD = minTempTupleRDD.reduceByKey((x, y) => Math.min(x, y))

    /**
     * Collect the result
     */
    val results = minTempByStationRDD.collect()

    /**
     * Printout the result in sorted way
     */
    for (result <- results.sorted) {
      val station = result._1
      val temp = result._2
      val formattedTemp = f"$temp%.2f F"
      println(s"$station minimum temprature: $formattedTemp")
    }

    /**
     * Stop spark Context
     */
    sparkContext.stop()
  }
}