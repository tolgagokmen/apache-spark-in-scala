package com.hsahu.spark.advance

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.spark.rdd._
import org.apache.spark.util.LongAccumulator
import org.apache.log4j._
import scala.collection.mutable.ArrayBuffer

/**
 * Breadth first search
 * https://en.wikipedia.org/wiki/Six_degrees_of_separation
 */
object degreeOfSeperationBFS {

  /*Starting node*/
  val startCharacterId = 5306
  /*Target Node*/
  val targetCharacterId = 14

  /*Global accumulator*/
  var hitCounter: Option[LongAccumulator] = None

  /*customData type ArrayOfConnections, distance, color */
  type BFSData = Tuple3[Array[Int], Int, String]
  /*heroID, BFSData*/
  type BFSNode = Tuple2[Int, BFSData]

  /*Create BFSNode*/
  def convertToBFS(line: String): BFSNode = {

    val fields = line.split("\\s+")

    val heroId = fields(0).toInt

    val connectionArray = fields.tail.map(x => x.toInt).toArray

    var distance: Int = 9999

    var color: String = "WHITE"

    if (heroId == startCharacterId) {
      color = "GRAY"
      distance = 0
    }

    return Tuple2.apply(heroId, Tuple3.apply(connectionArray, distance, color))
  }

  def createStartingRDD(sparkContext: SparkContext): RDD[BFSNode] = {

    val inputFile = sparkContext.textFile("data/Marvel-graph.txt")

    return inputFile.map(convertToBFS)
  }

  def bfsMap(node: BFSNode): Array[BFSNode] = {

    val characterId = node._1
    val data: BFSData = node._2

    val connectionArray: Array[Int] = data._1
    val distance: Int = data._2
    var color: String = data._3

    /*This is called from flatMap, so we return an array containing children if potentially many BFDNodes to add to our new RDD*/
    var results: ArrayBuffer[BFSNode] = ArrayBuffer()

    /*Gray nodes are flagged for expansion and create new gray nodes for each connection*/
    if (color == "GRAY") {
      for (connection <- connectionArray) {
        val newCharacterId = connection
        val newDistance = distance + 1
        val newColor = "GRAY"

        if (targetCharacterId == newCharacterId) {
          if (hitCounter.isDefined) {
            hitCounter.get.add(1)
          }
        }

        val newEntry: BFSNode = Tuple2.apply(newCharacterId, Tuple3.apply(Array(), newDistance, newColor))
        results += newEntry
      }

      color = "BLACK"
    }

    /*Add the original node back in so its connections can get merged with the gray nodes in the reducer*/
    val thisEntry: BFSNode = Tuple2.apply(characterId, Tuple3.apply(connectionArray, distance, color))
    results += thisEntry

    return results.toArray
  }

  def bfsReduce(nodeA: BFSData, nodeB: BFSData): BFSData = {
    // nodeA and nodeB are BFSData of same key
    val edgesA: Array[Int] = nodeA._1
    val edgesB: Array[Int] = nodeB._1
    val distanceA: Int = nodeA._2
    val distanceB: Int = nodeB._2
    val colorA: String = nodeA._3
    val colorB: String = nodeB._3

    // default node value
    var distance: Int = 9999
    var color: String = "WHITE"
    var edges: ArrayBuffer[Int] = ArrayBuffer()

    // merge all the connections
    if (edgesA.length > 0) {
      edges = edges ++ edgesA
    }
    if (edgesB.length > 0) {
      edges = edges ++ edgesB
    }

    // preserve the minimum distance
    distance = Math.min(distance, Math.min(distanceA, distanceB))

    // preserve darkest color
    if (colorA.equals("WHITE") && (colorB.equals("GRAY") || colorB.equals("BLACK")) || (colorA.equals("GRAY") && colorB.equals("BLACK"))) {
      color = colorB
    }
    if (colorB.equals("WHITE") && (colorA.equals("GRAY") || colorA.equals("BLACK")) || (colorB.equals("GRAY") && colorA.equals("BLACK"))) {
      color = colorA
    }

    return Tuple3.apply(edges.toArray, distance, color)

  }

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sparkContext = new SparkContext("local[*]", "degreeOfSeperationBFS")

    hitCounter = Some(sparkContext.longAccumulator("Hit Counter"))

    var iterationRDD = createStartingRDD(sparkContext)

    var iteration: Int = 0

    for (iteration <- 1 to 10) {
      println("Running BFS Iteration #" + iteration)

      /**
       * Create new vertices as needed to darken or reduce distances in the reduce stage
       * If we encounter the node we're looking for as a GRAY node
       * increment our accumulator to signal that we're done
       */
      val mapped = iterationRDD.flatMap(bfsMap)

      println("Processing " + mapped.count() + " values.")

      if (hitCounter.isDefined) {
        val hitCount = hitCounter.get.value
        if (hitCount > 0) {
          println("Hit the target character! From " + hitCount + " different direction(s).")
          return
        }
      }

      /**
       * Reducer combines data for each character id, preserving the darkest color and shortest path
       */
      iterationRDD = mapped.reduceByKey(bfsReduce)
    }

    sparkContext.stop()
  }
}
