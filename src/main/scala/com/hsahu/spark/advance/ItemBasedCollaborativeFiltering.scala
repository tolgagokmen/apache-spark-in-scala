package com.hsahu.spark.advance

import org.apache.spark._
import org.apache.spark.rdd._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import scala.io.Codec
import scala.io.Source
import java.nio.charset.CodingErrorAction
import scala.math.sqrt
import scala.collection.mutable.ArrayBuffer

/**
 * http://www.cs.carleton.edu/cs_comps/0607/recommend/recommender/itembased.html
 */
object ItemBasedCollaborativeFiltering {

  type MovieRating = (Int, Double)
  type UserRatingPair = (Int, (MovieRating, MovieRating))
  type RatingPair = (Double, Double)
  type RatingPairs = Iterable[RatingPair]

  /* Load up a Map of movie IDs to movie names. */
  def loadMovieNames(): Map[Int, String] = {

    implicit val codec = Codec("UTF-8")
    codec.onMalformedInput(CodingErrorAction.REPLACE)
    codec.onUnmappableCharacter(CodingErrorAction.REPLACE)

    // Create a Map of movieID -> movieName
    var movieNames: Map[Int, String] = Map()

    val lines = Source.fromFile("data/u.item").getLines()
    for (line <- lines) {
      var fields = line.split('|')
      if (fields.length > 1) {
        movieNames += (fields(0).toInt -> fields(1))
      }
    }
    return movieNames
  }

  /* CosineSimilarity = (X.Y)/(norm(X).norm(Y)) */
  def computeCosineSimilarity(ratingPairs: RatingPairs): Tuple2[Double, Int] = {
    var numPairs: Int = 0
    var score: Double = 0.0

    var sum_xx: Double = 0.0
    var sum_yy: Double = 0.0
    var sum_xy: Double = 0.0

    for (ratingPair <- ratingPairs) {
      val ratingX = ratingPair._1
      val ratingY = ratingPair._2

      sum_xx += ratingX * ratingX
      sum_yy += ratingY * ratingY
      sum_xy += ratingX * ratingY

      numPairs += 1
    }

    val numerator: Double = sum_xy
    val denominator = sqrt(sum_xx) * sqrt(sum_yy)

    if (denominator != 0) {
      score = numerator / denominator
    }

    return (score, numPairs)
  }

  /* Create RDD of (UserID, (movieID, movieRating)) */
  def loadUserMovieRatings(sparkContext: SparkContext): RDD[Tuple2[Int, Tuple2[Int, Double]]] = {

    val data = sparkContext.textFile("data/u.data")

    return data.map(x => x.split("\t")).map(x => (x(0).toInt, (x(1).toInt, x(2).toDouble)))
  }

  /*Filter out duplicate pairs (x,y) and (y,x) are same so take only one*/
  def filterDuplicates(userRatings: UserRatingPair): Boolean = {
    val movieRating1 = userRatings._2._1
    val movieRating2 = userRatings._2._2

    val movie1 = movieRating1._1
    val movie2 = movieRating2._1

    return movie1 < movie2
  }

  /*Transform RDD from (UserID, ((movieId1, movieRating1), (movieId2, movieRating2))) 
   * to ((movieId1, movieId2), (movieRating1, movieRating2))*/
  def makePairs(userRatings: UserRatingPair): Tuple2[Tuple2[Int, Int], Tuple2[Double, Double]] = {

    val movieRating1 = userRatings._2._1
    val movieRating2 = userRatings._2._2

    val movie1 = movieRating1._1
    val movie2 = movieRating2._1

    val rating1 = movieRating1._2
    val rating2 = movieRating2._2

    return Tuple2.apply(Tuple2.apply(movie1, movie2), Tuple2.apply(rating1, rating2))
  }

  /*Print top k movie similar to movieID*/
  def printTopSimilarMovies(nameDict: Map[Int, String], moviePairSimilarities: RDD[Tuple2[Tuple2[Int, Int], Tuple2[Double, Int]]], movieID: Int, takeValue: Int): Unit = {

    val scoreThreshold: Double = 0.97
    val coOccurenceThreshold: Double = 50.0

    /* Filter for movies with this similarity that are "good" as defined by our quality thresholds above */
    val filteredResults = moviePairSimilarities.filter(x =>
      {
        val pair = x._1
        val sim = x._2
        (pair._1 == movieID || pair._2 == movieID) && sim._1 > scoreThreshold && sim._2 > coOccurenceThreshold
      })

    // Sort by quality score.
    val results = filteredResults.map(x => (x._2, x._1)).sortByKey(false).take(takeValue)

    println("Top " + takeValue + " movies similar to " + nameDict(movieID))

    results.foreach(x => {
      var movies = x._2
      val score = x._1._1
      val strength = x._1._2

      var similarMovieId = movies._1
      // one of the movie in pair will be similar movie to movieID
      if (similarMovieId == movieID) {
        similarMovieId = movies._2
      }

      println(nameDict(similarMovieId) + "\tStrength: " + strength + "\tScore: " + score)
    })
  }

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sparkContext = new SparkContext("local[*]", "CollaborativeFiltering_ItemBased")

    /*(movie IDs, movie names)*/
    val nameDict = loadMovieNames()

    /*Validate passed argument*/
    if (args.length >= 1) {
      val movieID: Int = args(0).toInt
      if (!nameDict.contains(movieID)) {
        println("ERROR!! No such movie found with ID: " + movieID)
        return
      }
    } else {
      println("please provide a movieID")
      return
    }

    /*(UserID, (movieId1, movieRating1))  with duplicate key*/
    val ratings = loadUserMovieRatings(sparkContext)

    /*(UserID, ((movieId1, movieRating1), (movieId2, movieRating2))) with duplicate key*/
    /*Self-join to find every combination.*/
    /*it generate all the possible values of pair for all the movie that a user has seen*/
    val joinedRatings = ratings.join(ratings)

    /*(UserID, ((movieId1, movieRating1), (movieId2, movieRating2))) with multiple key without duplicate*/
    val uniqueJoinedRatings = joinedRatings.filter(filterDuplicates)

    /*((movieId1, movieId2), (movieRating1, movieRating2))*/
    val moviePairs = uniqueJoinedRatings.map(makePairs)

    /*((movieId1, movieId2), Iterable [(movieRating1, movieRating2)]) without duplicate key*/
    val moviePairRatings = moviePairs.groupByKey()

    /*((movieId1, movieId2), (Similarity, numberOfPairs))*/
    /*Apply cosine similarity*/
    val moviePairSimilarities = moviePairRatings.mapValues(computeCosineSimilarity).cache()

    /****************************************** TEST *******************************************/
    val movieID = args(0).toInt
    printTopSimilarMovies(nameDict, moviePairSimilarities, movieID, 10)

    sparkContext.stop()
  }
}