package com.hsahu.spark

import org.apache.spark._
import org.apache.spark.SparkContext._
import org.apache.log4j._
import javax.annotation.RegEx

object WordCountSorted {

  def main(args: Array[String]) {

    Logger.getLogger("org").setLevel(Level.ERROR)

    val sparkContext = new SparkContext("local[*]", "Word Count Sorted By occurance Using FlatMap")

    val lines = sparkContext.textFile("data/countwords.txt")

    /**
     * Match only word character and convert to lowerCase
     */
    val lowerCaseWords = lines.flatMap(x => x.split("\\W+")).map(x => x.toLowerCase())

    /**
     * Count word by frequency
     */
    val wordCountByFrequency = lowerCaseWords.map(x => (x, 1)).reduceByKey((x, y) => x + y)

    /**
     * reverse the order of tuple (word, frequency) to (frequency, word)
     * sort the RDD in descending order by key (frequency)
     */
    val wordCountSorted = wordCountByFrequency.map(x => (x._2, x._1)).sortByKey(false)

    /**
     * again reverse the order of tuple (frequency, word) to  (word, frequency)
     */
    val wordCountSortedByFrequency = wordCountSorted.map(x => (x._2, x._1))

    val results = wordCountSortedByFrequency.collect()

    for (result <- results) {
      val word = result._1
      val frequency = result._2
      println(s"$word - $frequency")
    }

    sparkContext.stop()
  }
}